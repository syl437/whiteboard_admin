import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    users: Array<any>;
    points: Array<any>;
    deleteModal: any;
    itemToDelete: any;
    editMode: boolean = false;
    editRow: any = null;
    fieldsToEdit = {name: '', phone: ''};
    createModal: any;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
    });

    constructor(public restangular: Restangular, public modalService: NgbModal, public fb: FormBuilder) { }

    async ngOnInit() {
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        const temp = this.users.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || d.email && d.email.toLowerCase().indexOf(val) !== -1 ||!val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    switchToEditMode (item) {
        this.editMode = true;
        this.editRow = item;
        this.fieldsToEdit.name = item.name;
        this.fieldsToEdit.phone = item.phone;
    }

    async editItem () {
        this.editRow.name = this.fieldsToEdit.name;
        this.editRow.phone = this.fieldsToEdit.phone;
        await this.editRow.patch().toPromise();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
        this.cancelEdit();
    }

    cancelEdit () {
        this.editMode = false;
        this.ngOnInit();
    }


    openCreateModal (content) {
        this.createModal = this.modalService.open(content);
    }

    async createItem (){
        let item = this.restangular.restangularizeElement('', {name: this.form.value.name, email: this.form.value.email, phone: this.form.value.phone}, 'users');
        await item.save().toPromise();
        this.createModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }


}
