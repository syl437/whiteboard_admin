import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';

export const JourneysRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Journeys'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New journey'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Edit journey'},
        }
    ]
}];
