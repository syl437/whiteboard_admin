import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {IndexComponent} from './index/index.component';

import {FileUploadModule} from 'ng2-file-upload/ng2-file-upload';
import {TreeModule} from 'angular-tree-component';
import {CustomFormsModule} from 'ng2-validation';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {TextMaskModule} from 'angular2-text-mask';
import {LoadingModule} from 'ngx-loading';
import {MomentModule} from 'angular2-moment';
import {UserJourneysRoutes} from './user_journeys.routing';
import {ShowComponent} from './show/show.component';
import {SidebarModule} from 'ng-sidebar';

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(UserJourneysRoutes),
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbProgressbarModule,
        CustomFormsModule,
        TreeModule,
        FileUploadModule,
        LoadingModule,
        TextMaskModule,
        MomentModule,
        NgbModule,
        SidebarModule.forRoot()
    ],
    declarations: [IndexComponent, ShowComponent]
})

export class UserJourneysModule {}